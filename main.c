//
// Created by mads on 9/20/20.
//

#include "malloc.h"
#include "stdbool.h"
#include "process_input.h"
#include "io.h"

int main() {
  int counter = 0;
  struct Node *tail = NULL;
  bool cont = true;

  while (cont) {
    char input_char;
    syscall_read(&input_char, 1);
    if (!process_input(input_char, &counter, &tail, &cont)) {
      linked_list_free(tail);
      return 1;
    }
  }

  linked_list_free(tail);
  return 0;
}