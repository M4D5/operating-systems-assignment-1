//
// Created by mads on 9/20/20.
//

#include "process_input.h"
#include "linked_list_tests.h"
#include "malloc.h"

bool main_test_1() {
  int counter = 0;
  bool cont = true;
  struct Node *tail = NULL;
  if (!process_input('q', &counter, &tail, &cont)) return false;

  // Check the value
  if (cont) return false;
  if (tail != NULL) return false;
  if (counter != 0) return false;

  return true;
}

bool main_test_2() {
  int counter = 0;
  bool cont = true;
  struct Node *tail = NULL;

  if (!process_input('a', &counter, &tail, &cont)) return false;
  if (!process_input('b', &counter, &tail, &cont)) return false;
  if (!process_input('b', &counter, &tail, &cont)) return false;
  if (!process_input('b', &counter, &tail, &cont)) return false;
  if (!process_input('a', &counter, &tail, &cont)) return false;
  if (!process_input('q', &counter, &tail, &cont)) return false;

  if (cont) return false;

  // Iterate through values and check them
  struct Node *itr = tail;
  if (itr->value != 4) return false;
  itr = itr->parent;
  if (itr->value != 0) return false;

  linked_list_free(tail);

  return true;
}

bool main_test_3() {
  int counter = 0;
  bool cont = true;
  struct Node *tail = NULL;

  if (!process_input('a', &counter, &tail, &cont)) return false;
  if (!process_input('b', &counter, &tail, &cont)) return false;
  if (!process_input('c', &counter, &tail, &cont)) return false;
  if (!process_input('a', &counter, &tail, &cont)) return false;
  if (!process_input('b', &counter, &tail, &cont)) return false;
  if (!process_input('c', &counter, &tail, &cont)) return false;
  if (!process_input('a', &counter, &tail, &cont)) return false;
  if (!process_input('q', &counter, &tail, &cont)) return false;

  // Check the values
  if (cont) return false;
  if (counter != 7) return false;
  if (tail->value != 6) return false;

  linked_list_free(tail);

  return true;
}

bool main_tests() {
  if (!main_test_1()) {
    printf("main_test_1 failed\n");
    return false;
  } else {
    printf("main_test_1 passed\n");
  }

  if (!main_test_2()) {
    printf("main_test_2 failed\n");
    return false;
  } else {
    printf("main_test_2 passed\n");
  }

  if (!main_test_3()) {
    printf("main_test_3 failed\n");
    return false;
  } else {
    printf("main_test_3 passed\n");
  }

  return true;
}