//
// Created by mads on 9/20/20.
//

#ifndef OPERATING_SYSTEMS_MAIN_TESTS_H
#define OPERATING_SYSTEMS_MAIN_TESTS_H

#include "process_input.h"
#include <stdbool.h>

bool main_tests();

#endif //OPERATING_SYSTEMS_MAIN_TESTS_H
