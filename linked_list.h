//
// Created by mads on 9/14/20.
//

#ifndef OPERATING_SYSTEMS_LINKED_LIST_H
#define OPERATING_SYSTEMS_LINKED_LIST_H

struct Node {
    int value;
    struct Node* parent;
};

bool linked_list_add(int value, struct Node* tail, struct Node** new_node);

void linked_list_remove_last(struct Node** tail);

void linked_list_free(struct Node* node);

void linked_list_print(struct Node* node);

#endif //OPERATING_SYSTEMS_LINKED_LIST_H
