//
// Created by mads on 9/14/20.
//
#include "stdbool.h"
#include "malloc.h"
#include "io.h"
#include "linked_list.h"

bool linked_list_add(int value, struct Node *tail, struct Node **new_node) {
  *new_node = malloc(sizeof(struct Node));

  if (new_node == NULL) {
    perror("Failed to allocate memory segment for node");
    return false;
  }

  (*new_node)->value = value;
  (*new_node)->parent = tail;

  return true;
}

void linked_list_remove_last(struct Node **tail) {
  if (*tail == NULL) return;

  struct Node* old_tail = *tail;

  if ((*tail)->parent != NULL) {
    *tail = (*tail)->parent;
  } else {
    *tail = NULL;
  }

  free(old_tail);
}

void linked_list_free(struct Node *node) {
  if (node == NULL) {
    return;
  }

  if (node->parent != NULL) {
    linked_list_free(node->parent);
  }

  free(node);
}

void linked_list_print_internal(struct Node *node) {
  if (node->parent != NULL) {
    linked_list_print_internal(node->parent);
  }

  write_int(node->value);
  write_char(',');
}

void linked_list_print(struct Node *node) {
  if (node == NULL) {
    write_char('\n');
    return;
  }

  if (node->parent != NULL) {
    linked_list_print_internal(node->parent);
  }

  write_int(node->value);
  write_char('\n');
}
