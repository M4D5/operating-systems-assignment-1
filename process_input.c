//
// Created by mads on 9/20/20.
//

#include "process_input.h"

/**
 * Handles the command specified by input_char, and updates counter, tail and cont accordingly.
 * Returns true, if the operation was successful, false if it was not.
 */
bool process_input(char input_char, int *counter, struct Node **tail, bool *cont) {
  if (input_char == 'a') {
    if (!linked_list_add(*counter, *tail, tail)) return false;
  } else if (input_char == 'c') {
    linked_list_remove_last(tail);
  } else if (input_char != 'b') {
    // Print the list and stop (cont = false)
    linked_list_print(*tail);
    *cont = false;
    return true;
  }

  *counter += 1;
  *cont = true;
  return true;
}