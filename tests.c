//
// Created by mads on 9/20/20.
//

#include "linked_list_tests.h"
#include "main_tests.h"

int main() {
  if (!linked_list_tests()) {
    return 1;
  }

  if (!main_tests()) {
    return 1;
  }

  return 0;
}