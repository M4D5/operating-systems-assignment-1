OBJS		= process_input.o linked_list.o main.o io.o
TEST_OBJS	= linked_list_tests.o linked_list.o main_tests.o tests.o process_input.o io.o

OUT			= assignment
TEST_OUT 	= assignment_tests
CC	 		= gcc

$(OUT): $(OBJS)
	$(CC) -g $(OBJS) -o $(OUT) $(LFLAGS)
	
$(TEST_OUT): $(TEST_OBJS)
	$(CC) -g $(TEST_OBJS) -o $(TEST_OUT) $(LFLAGS)

clean:
	rm -f $(OBJS) $(OUT) $(TEST_OBJS) $(TEST_OUT)

# run the program
run: $(OUT)
	./$(OUT)
	
# run tests
run_tests: $(TEST_OUT)
	./$(TEST_OUT)