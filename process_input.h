//
// Created by mads on 9/20/20.
//

#ifndef OPERATING_SYSTEMS_PROCESS_INPUT_H
#define OPERATING_SYSTEMS_PROCESS_INPUT_H

#include <stdbool.h>
#include "linked_list.h"

bool process_input(char input_char, int *counter, struct Node **tail, bool* cont);

#endif //OPERATING_SYSTEMS_PROCESS_INPUT_H
