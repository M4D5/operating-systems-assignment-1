//
// Created by mads on 9/20/20.
//

#ifndef OPERATING_SYSTEMS_LINKED_LIST_TESTS_H
#define OPERATING_SYSTEMS_LINKED_LIST_TESTS_H

#include <stdbool.h>

bool linked_list_tests();

#endif //OPERATING_SYSTEMS_LINKED_LIST_TESTS_H
