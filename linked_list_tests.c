//
// Created by mads on 9/14/20.
//
#include "linked_list_tests.h"
#include "stdbool.h"
#include "malloc.h"
#include "linked_list.h"

int test_add() {
  struct Node *tail = NULL;

  // Fill the collection
  if (!linked_list_add(1, NULL, &tail)) return false;
  if (!linked_list_add(2, tail, &tail)) return false;
  if (!linked_list_add(3, tail, &tail)) return false;

  // Iterate through values and check them
  struct Node *itr = tail;
  if (itr->value != 3) return false;
  itr = itr->parent;
  if (itr->value != 2) return false;
  itr = itr->parent;
  if (itr->value != 1) return false;

  linked_list_free(tail);

  return true;
}

int test_remove_last() {
  struct Node *tail = NULL;

  // Make a collection of length 1
  if (!linked_list_add(1, NULL, &tail)) return false;

  // Remove the only element
  linked_list_remove_last(&tail);

  // tail should be null (list length == 0)
  if (tail != NULL) return false;

  // Add 2 elements
  if (!linked_list_add(1, NULL, &tail)) return false;
  if (!linked_list_add(2, tail, &tail)) return false;

  // Remove 1 element
  linked_list_remove_last(&tail);

  // Value should be 1 and parent node to the first node should be null
  if (tail->value != 1 || tail->parent != NULL) return false;

  // Add 2 more elements (length should be 3)
  if (!linked_list_add(2, tail, &tail)) return false;
  if (!linked_list_add(3, tail, &tail)) return false;

  // Remove 1 element
  linked_list_remove_last(&tail);

  // Value of tail should be 2, and parent value should be 1
  if (tail->value != 2 || tail->parent == NULL || tail->parent->value != 1) return false;

  linked_list_free(tail);

  return true;
}

bool linked_list_tests() {
  if (!test_add()) {
    printf("test_add passed\n");
    return false;
  } else {
    printf("test_add passed\n");
  }

  if (!test_remove_last()) {
    printf("test_remove_last failed\n");
    return false;
  } else {
    printf("test_remove_last passed\n");
  }

  return true;
}
